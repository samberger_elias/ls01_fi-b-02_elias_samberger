
public class Ausgabeformatierung_Aufgabe3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.printf("%-12s |","Fahrenheit");
		System.out.printf("%10s" ,"Celsius\n");
		System.out.printf("%s%n","-------------------------");
		System.out.printf("%-12s |",-20);
		System.out.printf("%10s\n" ,-28.89);
		System.out.printf("%-12s |",-10);
		System.out.printf("%10s\n" ,-23.33);
		System.out.printf("+%-11s |",0);
		System.out.printf("%10s\n" ,-17.78);
		System.out.printf("+%-11s |",20);
		System.out.printf("%10s\n" ,-6.67);
		System.out.printf("+%-11s |",30);
		System.out.printf("%10s\n" ,-1.11);
		

	}

}
