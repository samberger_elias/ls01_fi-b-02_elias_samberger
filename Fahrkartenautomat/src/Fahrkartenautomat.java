﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double rückgabebetrag;
       
       //Fahrkarte auswählen
       //======================
       zuZahlenderBetrag = fahrkartenWählen(tastatur);
      
       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = fahrkartenBezahlen(tastatur, zuZahlenderBetrag);

       // Fahrscheinausgabe
       // -----------------
       fahrkartenAusgeben(tastatur);
       
       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = rueckgeldAusgeben(tastatur, eingezahlterGesamtbetrag);
       
    }

	private static double rueckgeldAusgeben(Scanner tastatur, double rückgabebetrag) {
		
		   double eingezahlterGesamtbetrag;
		   double zuZahlenderBetrag;
		
	       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       if(rückgabebetrag > 0.0)
	       {
	    	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	           }
	       }

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.");
		
		return 0;
	}

	private static void fahrkartenAusgeben(Scanner tastatur) {
		
	       System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
		
	}

	static double fahrkartenBezahlen(Scanner geldeinwurf, double zuZahlen) {
		
		double eingezahlt = 0.0;
		double eingeworfeneMünze;
		eingezahlt = 0.0;
	       while(eingezahlt < zuZahlen)
	       {
	    	   System.out.print("Noch zu zahlen: " );
	    	   System.out.printf("%2.2f",zuZahlen - eingezahlt);
	    	   System.out.print(" Euro\n");
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   eingeworfeneMünze = geldeinwurf.nextDouble();
	           eingezahlt += eingeworfeneMünze;
	       }	
		
		return eingezahlt;
	}

	static double fahrkartenWählen(Scanner eingabe) {
		
		double zuZahlenderBetrag;
		int anzahl;
		double gesamtpreis;
		
		 System.out.print("Zu zahlender Betrag (EURO): ");
	       zuZahlenderBetrag = eingabe.nextDouble();
	       System.out.print("Gewünschte Anzahl der Fahrkarten: ");
	       anzahl = eingabe.nextInt();
	       
	       gesamtpreis = anzahl * zuZahlenderBetrag;

		return gesamtpreis;
	}
}