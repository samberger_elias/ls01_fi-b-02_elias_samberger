
public class AusgabenformatierungBeispiele {

	public static void main(String[] args) {
		
//		System.out.print("hello");
//		System.out.print(" du \"Alex"\" ");
        
		// String
		System.out.printf("|%s|%n","Elias");
		System.out.printf("|%20s|\n","Elias");
		System.out.printf("|-%20s|\n","Elias");
		System.out.printf("|-%20.3s|\n","Elias");
	
		
		System.out.printf("|%f|%n",123456.789);
		System.out.printf("|%.2f|%n",123456.789);
				
	}

}
